from datetime import datetime as dt
from flask import abort, flash, g, json, redirect, render_template, request, \
    session
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug import generate_password_hash
from app import app, lm, sentry
from app.database import db_session
from .config import DEBUG, APP_NAME
from .constants import Status, Privacy
from .emails import *
from .exceptions import MultipleBookingsError
from .forms import *
from .models import User, Course, Workshop, Address, Attendance, Booking, \
    Question
from .utils import friendly_url, tz_to_utc, utc_to_tz, aware_utcnow
from .momentjs import moment


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.before_request
def before_request():
    session.modified = True
    g.debug = DEBUG
    g.user = current_user


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.route('/')
def index():
    return render_template('index.html', active='index',
                           title='{} - Run great courses'.format(APP_NAME))


@app.route('/course/', methods=['GET', 'POST'])
@app.route('/course/<course_id>/', methods=['GET', 'POST'])
@login_required
def course(course_id=None):
    # course_id should only be set when editing an existing course.
    if course_id:
        try:
            course = g.user.get_course(course_id)
        except KeyError:
            abort(404)
        title = u'Edit {}'.format(course.name)
    else:
        course = None
        title = 'Create your course'
    form = CourseForm()
    newclass = render_template('forms/fragments/class.html',
                               workshop=WorkshopForm())

    # 1) Editing an existing course, GET, course_id set.
    if request.method == 'GET' and course_id:
        form.course_name.data = course.name
        form.description.data = course.description
        form.course_location.addr_name.data = course.address.name
        form.course_location.addr_line1.data = course.address.line1
        form.course_location.addr_line2.data = course.address.line2
        form.course_location.addr_city.data = course.address.city
        form.course_location.addr_region.data = course.address.region
        form.course_location.addr_country.data = course.address.country
        form.course_location.addr_postcode.data = course.address.postcode
        # If course already has associated workshops remove form default.
        if len(course.workshops) >= 1:
            form.classes.pop_entry()
        # Append workshops to course to be displayed.
        for workshop in course.workshops:
            w = WorkshopForm()
            w.class_id = workshop.id
            w.class_name = workshop.name
            w.class_description = workshop.description
            w.class_places = workshop.places
            w.class_max_places = workshop.max_places
            w.class_price = '{:.2f}'.format(workshop.price)
            # Convert stored UTC datetime to user's local timezone.
            start = utc_to_tz(workshop.start, g.user.timezone)
            w.class_date = start.strftime('%d/%m/%Y')
            w.class_start = str(start.hour)
            w.class_end = str(utc_to_tz(workshop.end, g.user.timezone).hour)
            form.classes.append_entry(w)

    # 2) Saving an edited or new course, POST, course_id:edited, None:new.
    if form.validate_on_submit():
        action = 'edited'
        if not course_id:
            action = 'created'
            course = Course()
        course.name = form.course_name.data
        course.description = form.description.data
        country = None
        if form.course_location.addr_country.data != 'Select a country':
            country = form.course_location.addr_country.data
        course.address = Address(
            name=form.course_location.addr_name.data,
            line1=form.course_location.addr_line1.data,
            line2=form.course_location.addr_line2.data,
            city=form.course_location.addr_city.data,
            region=form.course_location.addr_region.data,
            postcode=form.course_location.addr_postcode.data,
            country=country)
        course.user_id = g.user.get_id()

        # Prevent creation of course without classes.
        if len(form.classes.data) < 1:
            flash('The course must have at least one class.', 'error')
            return redirect(url_for('course', course_id=course_id))

        # Delete any original classes not contained in new POSTed data.
        old_classes = {w.id for w in course.workshops}
        new_classes = {w['class_id'] for w in form.classes.data}
        to_delete = old_classes - new_classes
        for idx, workshop in enumerate(course.workshops):
            if workshop.id in to_delete:
                del(course.workshops[idx])

        # Populate or create course classes with submitted data.
        for w in form.classes.data:
            # Workshops (currently) always start and end on same day.
            try:
                workshop = course.get_workshop(w['class_id'])
            except KeyError:
                workshop = Workshop()
            workshop.name = w['class_name']
            workshop.description = w['class_description']
            workshop.places = w['class_places']
            workshop.max_places = w['class_max_places']
            workshop.price = w['class_price']

            # Localise and convert dt objects to UTC, then make naive for db.
            date = dt.strptime(w['class_date'], '%d/%m/%Y')
            start = date.replace(hour=w['class_start'])
            end = date.replace(hour=w['class_end'])
            workshop.start = tz_to_utc(start, g.user.timezone)
            workshop.end = tz_to_utc(end, g.user.timezone)
            course.workshops.append(workshop)

        # Model defaults to PRIVATE
        # course.privacy = form.privacy.data
        if 'make_live' in request.form.keys():
            course.set_active()

        # Course successfully created, save in database and redirect.
        db_session.add(course)
        db_session.commit()
        flash(u'{} has been {}.'.format(course.name, action))
        return redirect(url_for('course_details', course_id=course.identifier))
    elif form.errors:
        if 'CSRF token missing' in form.errors.get('csrf_token', []):
            flash('The form security token has expired.  Please reload the '
                  'page and try again.', 'error')
        else:
            flash('The course could not be saved because of errors in the '
                  'form.', 'error')

    # Active is used to enable the class js on layout.html, don't remove it.
    return render_template('forms/course_create.html', form=form,
                           course=course, title=title, active='course',
                           newclass=newclass)


@app.route('/course/<course_id>/details', methods=['GET', 'POST'])
@login_required
def course_details(course_id=None):
    # Nope the fuck out if course_id unset or not one of the user's courses.
    try:
        course = g.user.get_course(course_id)
    except KeyError:
        abort(404)

    form = CourseDetailsForm()
    newquestion = render_template('forms/fragments/question.html',
                                  question=AttendeeQuestion())

    if request.method == 'GET':
        # Convert stored UTC datetimes to user's local timezone.
        booking_open = utc_to_tz(course.booking_open, g.user.timezone)
        form.booking_open.date.data = booking_open.strftime('%d/%m/%Y')
        form.booking_open.time.data = booking_open.hour
        if course.booking_close:
            booking_close = utc_to_tz(course.booking_close, g.user.timezone)
            form.booking_close.date.data = booking_close.strftime('%d/%m/%Y')
            form.booking_close.time.data = booking_close.hour

        # Append questions to course to be displayed.
        for question in course.questions:
            bq = AttendeeQuestion()
            bq.question_id = question.id
            bq.question_name = question.name
            bq.question_required = question.required
            form.questions.append_entry(bq)

    # 2) Save additional attributes to course.
    if form.validate_on_submit():
        # Upload image to cloudinary.
        if form.image.data:
            course.set_image(form.image.data)
        # Set booking open.
        date = dt.strptime(form.booking_open.date.data, '%d/%m/%Y')
        date = date.replace(hour=form.booking_open.time.data)
        course.booking_open = tz_to_utc(date, g.user.timezone)
        # Set booking close.
        if form.booking_close.date.data:
            date = dt.strptime(form.booking_close.date.data, '%d/%m/%Y')
            date = date.replace(hour=form.booking_close.time.data)
            course.booking_close = tz_to_utc(date, g.user.timezone)

        # Delete any original questions not contained in new POSTed data.
        old_questions = {q.id for q in course.questions}
        new_questions = {q['question_id'] for q in form.questions.data}
        to_delete = old_questions - new_questions
        for idx, question in enumerate(course.questions):
            if question.id in to_delete:
                del(course.questions[idx])

        # Populate or create course booking questions with submitted data.
        for q in form.questions.data:
            try:
                question = course.get_question(q['question_id'])
            except KeyError:
                question = Question()
            question.name = q['question_name']
            question.required = q['question_required']
            course.questions.append(question)

        # Save course and inform course organiser.
        db_session.add(course)
        db_session.commit()
        flash(u'{} has been saved.'.format(course.name))
        return redirect(url_for('manage_course', course_id=course.identifier))
    elif form.errors:
        if 'CSRF token missing' in form.errors.get('csrf_token', []):
            flash('The form security token has expired.  Please reload the '
                  'page and try again.', 'error')
        else:
            flash('The course could not be saved because of errors in the '
                  'form.', 'error')

    return render_template('forms/course_details.html', form=form,
                           active='course_details', course=course,
                           newquestion=newquestion, title='Booking settings')


@app.route('/course/<course_id>/manage/')
@login_required
def manage_course(course_id):
    try:
        course = g.user.get_course(course_id)
    except BaseException:
        abort(404)
    return render_template('forms/course_manage.html', active='manage_course',
                           course=course,
                           title=u'Manage {}'.format(course.name))


@app.route('/course/<course_id>/<action>/')
@login_required
def change_course(course_id, action):
    try:
        # Ensure course belongs to current user and action is allowed.
        course = g.user.get_course(course_id)
        if action not in ['publish', 'cancel', 'public', 'private']:
            raise Exception('Invalid action specified.')
    except BaseException:
        abort(404)

    if action == 'publish':
        course.set_active()
    elif action == 'cancel':
        course.status = Status.INACTIVE.value
    elif action == 'public':
        course.privacy = Privacy.PUBLIC.value
    else:
        course.privacy = Privacy.PRIVATE.value
    db_session.add(course)
    db_session.commit()

    return redirect(url_for('manage_course', course_id=course_id))


@app.route('/class/<int:w_id>/cancel/')
@login_required
def workshop_cancel(w_id):
    # Ensure class exists and is managed by current user.
    try:
        w = Workshop.query.filter_by(id=w_id).first()
        w.cancel()
        db_session.add(w)
        db_session.commit()
        flash(u'{} has been cancelled.'.format(w.name))
    except Exception as e:
        flash('We couldn\'t find the specified class.')
        return redirect(url_for('my_courses'))
    return redirect(url_for('manage_course', course_id=w.course.identifier))


@app.route('/attendance/<int:a_id>/cancel/')
@login_required
def attendance_cancel(a_id):
    a = Attendance.query.filter_by(id=a_id).first()
    # Ensure attendance exists and attendance managed or owned by current user.
    managed_by_current_user = g.user.manages_attendance(a_id)
    owned_by_current_user = a_id in [a.id for a in g.user.active_attendances()]
    if a and managed_by_current_user or owned_by_current_user:
        name = '{}\'s'.format(a.user.name)
        url = url_for('manage_course', course_id=a.get_course().identifier)
        if a.user.id == g.user.id:
            name, url = 'Your', url_for('your_account')
        try:
            cancelled = a.cancel()
            db_session.add(cancelled)
            db_session.commit()
            flash(u'{} attendance for {} has been cancelled.'.format(
                name, a.workshop.name))
        except Exception as e:
            flash('{}'.format(e))
    else:
        flash('We couldn\'t find the specified attendance.')
    return redirect(url)


@app.route('/c/<course_id>/', defaults={'slug': None}, methods=['GET', 'POST'])
@app.route('/c/<course_id>/<slug>/', methods=['GET', 'POST'])
def show_course(course_id, slug):
    course = Course.query.filter_by(identifier=course_id).first()
    if not course:
        abort(404)
    # Allow only course creator to view course if course isn't active.
    if course.status != Status.ACTIVE.value:
        if not g.user.is_authenticated or g.user.id != course.user.id:
            abort(404)

    form = CourseRegistrationForm()

    # Populate registration form.
    if request.method == 'GET':
        # Redirect to an SEO and human friendly URL if available.
        friendly = friendly_url(course.name)
        if slug != friendly:
            return redirect(
                url_for('show_course', course_id=course_id, slug=friendly))

        # Flash message if course isn't open for booking yet.
        if course.booking_open and aware_utcnow() < course.booking_open:
            flash(u'{} booking opens on {}.'.format(
                course.name, moment(course.booking_open).format('llll')),
                'error')
        if course.booking_close and aware_utcnow() > course.booking_close:
            flash(u'{} has closed for booking.'.format(course.name), 'error')

        for workshop in course.workshops:
            w = CourseWorkshopForm()
            w.workshop_id = workshop.id
            w.quantity.choices = [
                (i, i) for i in range(0, workshop.max_places + 1)]
            form.workshops.append_entry(w)

    # Create new booking and redirect to registration page.
    if form.validate_on_submit():
        # Validate course is available for booking.
        try:
            course.can_be_booked()
        except Exception as e:
            flash(u'{}'.format(e))
            return redirect(url_for('show_course',
                                    course_id=course.identifier))

        # Use booking id from session if available and user not authenticated.
        if not g.user.is_authenticated:
            booking = Booking.query.filter_by(
                identifier=session.get('{}_booking_id'.format(course_id), None)
            ).first()
            # Create a new booking if couldn't find anything in db.
            booking = booking or Booking(course_id=course.id)
        elif g.user.course_bookings(course.id):
            # Authenticated user; has a booking for this course: use it.
            booking = g.user.course_bookings(course.id)[0]
        else:
            # Authenticated user; no booking: create new one.
            booking = Booking(user_id=g.user.id, course_id=course.id)

        # Delete any expired or inactive attendances on the booking.
        for a in booking.expired_and_inactive_attendances():
            db_session.delete(a)
        booking.status = Status.NEW.value
        db_session.commit()

        # Append an attendance for each workshop to be attended not already on
        # the booking.
        current_attendances = [a.workshop_id for a in booking.new_and_active()]
        to_attend = [w for w in form.workshops.data if w['workshop_id'] not in
                     current_attendances and w['quantity'] > 0]
        for w in to_attend:
            try:
                # Raise exception if course or workshop can't be booked.
                Workshop.get_workshop(id=w['workshop_id']).can_be_booked()
                # Workshop id is verified by new Attendance.
                booking.attendances.append(
                    Attendance(workshop_id=w['workshop_id'],
                               quantity=w['quantity']))
            except Exception as e:
                flash(u'{}'.format(e), 'error')
                return redirect(url_for('show_course',
                                        course_id=course.identifier))
        db_session.add(booking)
        db_session.commit()

        # If no user authenticated, add booking id to session for later reuse.
        if not g.user.is_authenticated:
            session['{}_booking_id'.format(course_id)] = booking.identifier

        return redirect(url_for('register', reference=booking.identifier))
    elif form.errors:
        if 'workshops' in form.errors:
            flash(form.errors['workshops'][0], 'error')
        if 'CSRF token missing' in form.errors.get('csrf_token', []):
            flash('The form security token has expired.  Please reload the '
                  'page and try again.', 'error')
        else:
            flash('There are errors in the class selection.', 'error')

    return render_template('forms/course_show.html', course=course, form=form,
                           active='show_course', title=course.name,
                           distraction_free=True)


@app.route('/book/<reference>/', methods=['GET', 'POST'])
def register(reference):
    # Display page for user to review their order.
    booking = Booking.query.filter_by(identifier=reference).first()
    if not booking:
        abort(404)
    course = booking.get_course()

    # Since we don't have functionality to edit existing bookings yet, redirect
    # the user away if they come to this page with an active booking.
    if booking.status == Status.ACTIVE.value:
        return redirect(url_for('booking_confirmation', reference=reference))

    # Run checks on the user, course, booking and attendances.
    try:
        booking.validate()
    except Exception as e:
        flash('{}'.format(e))
        return redirect(url_for('show_course', course_id=course.identifier))

    form = OrderReviewNewUserForm()

    # Asking for name and email is unnecessary if user is logged in.
    if g.user.is_authenticated:
        make_optional(form.name)
        make_optional(form.email)

    # 1) Populate registration form and add any additional booking information
    # questions to the form.
    if request.method == 'GET':
        for idx, question in enumerate(course.questions):
            q = OrderReviewNewUserQuestion.append_field(
                'question', StringField(question.name))
            form.questions.append_entry(q)
            form.questions.entries[idx].question.data = ''

    # 2) Save new course booking.
    if form.validate_on_submit():
        # Get user account or create new one if one doesn't exist.
        user = User.query.filter_by(email=form.email.data).first()
        if g.user.is_authenticated:
            user = g.user
        elif user is None:
            user = User(email=form.email.data, name=form.name.data)
            db_session.add(user)
            db_session.commit()

        # Set user on the booking and child attendances.
        user = User.query.filter_by(email=user.email).first()
        booking.set_user(user.id)

        # Re-run checks on the user, course, booking and attendances.
        try:
            booking.validate()
        except MultipleBookingsError as e:
            # Delete booking and attendances, tell user to login to book.
            for a in booking.attendances:
                db_session.delete(a)
            db_session.delete(booking)
            db_session.commit()
            flash('{}  Please <a href="{}">login</a> to update your booking.'
                  .format(e, url_for('login')), 'error')
            return redirect(
                url_for('show_course', course_id=course.identifier))
        except Exception as e:
            flash('{}'.format(e))
            return redirect(
                url_for('show_course', course_id=course.identifier))

        # Activate booking and child attendances, set/update booking price.
        booking.confirm()

        # Create dict of Q:A pairs and add to db as JSON.
        if form.questions.data:
            qa_pairs = {}
            for question, received in zip(course.questions,
                                          form.questions.data):
                qa_pairs[question.name] = received.get('question', None)
                if not qa_pairs[question.name]:
                    app.logger.debug(
                        'Couldn\'t extract question from received answered '
                        'questions.'
                    )
            booking.information = json.dumps(qa_pairs)

        db_session.add(booking)
        db_session.commit()
        # Send booking confirmation.
        send_booking_confirmation(booking.get_user().email, booking)
        # Remove booking_id from session since the booking has completed.
        session.pop('{}_booking_id'.format(course.identifier), None)
        return redirect(url_for('booking_confirmation', reference=reference))

    return render_template('forms/course_register.html', active='register',
                           course=course, booking=booking, form=form,
                           title=course.name, distraction_free=True)


@app.route('/bookingconfirmation/<reference>/', methods=['GET'])
def booking_confirmation(reference):
    booking = Booking.query.filter_by(identifier=reference).first()
    if not booking:
        abort(404)
    course = booking.get_course()
    return render_template('forms/booking_confirmation.html', course=course,
                           booking=booking, title=course.name)


@app.route('/bookingconfirmation/<reference>/resend/')
def booking_confirmation_resend(reference):
    booking = Booking.query.filter_by(identifier=reference).first()
    if not booking:
        abort(404)
    send_booking_confirmation(booking.get_user().email, booking)
    flash('We\'ve sent you another booking confirmation email.')
    return redirect(url_for('booking_confirmation',
                            reference=booking.identifier))


@app.route('/account/')
@app.route('/account/bookings/')
@login_required
def your_account():
    return render_template('account/bookings.html', active='your_account',
                           title='Your bookings')


@app.route('/account/bookings/<reference>/cancel/')
@login_required
def booking_cancel(reference):
    if reference in [b.identifier for b in g.user.active_bookings()]:
        booking = Booking.query.filter_by(identifier=reference).first()
        try:
            booking.cancel()
            db_session.add(booking)
            db_session.commit()
            flash(u'Your booking for {} has been cancelled.'
                  .format(booking.get_course().name))
        except Exception as e:
            flash('{}'.format(e))
    else:
        flash('The booking doesn\'t exist or is already cancelled.')
    return redirect(url_for('your_account'))


@app.route('/account/mycourses/')
@login_required
def my_courses():
    return render_template('account/my_courses.html', active='my_courses',
                           courses=g.user.courses, title='Your courses')


@app.route('/account/change_password/', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePassword()
    if form.validate_on_submit():
        g.user.password = generate_password_hash(form.newpassword.data)
        db_session.add(g.user)
        db_session.commit()
        flash('Password updated.')

    return render_template('account/change_password.html',
                           active='change_password', form=form,
                           title="My account")


@app.route('/signup/', methods=['GET', 'POST'])
def sign_up():
    if g.user.is_authenticated:
        return redirect(url_for('index'))

    form = Signup()
    title = 'Create an account'
    email = None

    # New users sent here from login form to complete account creation.
    new_user = User.query.filter_by(
        email=request.args.get('email', None),
        status=Status.NEW.value,
    ).first()
    if new_user:
        form.name.data = new_user.name
        form.email.data = new_user.email

    # Display "in-beta" message to users trying to sign up for Pro plan.
    plan = request.args.get('plan')
    if plan and plan == 'pro':
        flash('Hey there!  It looks like you caught us before we\'re quite '
              'ready.  We\'re currently gauging interest in Pro accounts '
              'before we go live with the feature set.  You can still create '
              'a free account and we\'ll notify you when we\'re ready to '
              'release.')

    if form.validate_on_submit():
        # Overwrite any status NEW account or create new user.
        user = form.get_user() or User()
        form.populate_obj(user)
        user.password = generate_password_hash(form.password.data)
        user.status = Status.ACTIVE.value
        user.last_auth = aware_utcnow()
        db_session.add(user)
        db_session.commit()
        # send email to welcome new customer
        send_register_email(user.email)
        email = user.email
        title = 'Thanks for signing up!'
        # Login user.
        if login_user(user):
            session.permanent = True
            flash(u'Your account has been activated. Welcome {}!'
                  .format(user.name))
            return redirect(url_for('my_courses'))
        flash('There was a problem logging you in - please contact support.',
              'error')

    return render_template('forms/signup.html', form=form, title=title,
                           email=email)


@app.route('/register/resend_register_email/')
def resend_register_email():
    email = request.args.get('email')
    user = User.query.filter_by(email=email, status=1).first()
    if user:
        send_register_email(email)
        flash('We\'ve sent you another welcome email.')
    else:
        flash('Invalid email your account is already active.', 'error')
    return redirect(url_for('index'))


@app.route('/account/verify_email/', defaults={'payload': None})
@app.route('/account/verify_email/<payload>/')
def verify_email(payload):
    # if the signature and payload are valid, set account status to 2:active
    try:
        email = link_is_authorised(payload, max_age=3600).get('email')
    except BaseException:
        flash('Invalid or expired URL, please contact support.', 'error')
        return redirect(url_for('index'))

    user = User.query.filter_by(email=email).first()
    user.status = Status.ACTIVE.value
    db_session.add(user)
    db_session.commit()
    if login_user(user):
        session.permanent = True
        user.last_auth = aware_utcnow()
        db_session.add(user)
        db_session.commit()
        flash(u'Your account has been activated. Welcome {}!'
              .format(user.name))
        return redirect(url_for('index'))
    flash('Unable to login, please contact support.', 'error')
    return redirect('login')


@app.route('/login/', methods=['GET', 'POST'])
def login():
    form = Login()

    if form.validate_on_submit():
        user = form.get_user()
        # If user has two-factor authentication setup.
        if user.tfa_enabled:
            # add user email to session
            session['email'] = user.email
            return redirect(url_for('verify_2fa'))
        if login_user(user):
            session.permanent = True
            user.last_auth = aware_utcnow()
            db_session.add(user)
            db_session.commit()
            # redirect to original destination, if logged-in en route
            return redirect(url_for('my_courses'))
        flash('There was a problem logging you in - please contact support.',
              'error')

    # Get user to finish account setup if they have partial account.
    if 'New user account or no password set' in form.errors.get('email', []):
        flash('Please set a password to complete your account setup.')
        return redirect(url_for('sign_up', email=form.email.data))

    return render_template('forms/login.html', form=form,
                           title='Login to your account')


@app.route('/forgot/', methods=['GET', 'POST'])
def forgot():
    form = ForgotPassword()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        # if there's a user with that email address
        if user:
            # send email containing itsdangerous link
            send_reset_link(user.email)
        # display same message whether or not email sent
        flash('We\'ve sent you a password reset link.  Please \
            check your inbox.')
        return redirect(url_for('index'))
    return render_template('forms/forgot.html', form=form,
                           title='Reset your password')


@app.route('/reset_password/<payload>/', methods=['GET', 'POST'])
def reset_password(payload):
    # Check the signature and payload are valid.
    if not link_is_authorised(payload, max_age=3600):
        flash('Invalid or expired URL.', 'error')
        return redirect(url_for('forgot'))

    form = ResetPassword()
    if form.validate_on_submit():
        email = link_is_authorised(payload, max_age=3600).get('email')
        user = User.query.filter_by(email=email).first()
        user.password = generate_password_hash(form.newpassword.data)
        db_session.add(user)
        db_session.commit()
        flash('Password updated.')

        # if user has two-factor authentication setup
        if user.tfa_enabled:
            return redirect(url_for('verify_2fa'))
        if login_user(user):
            session.permanent = True
            user.last_auth = aware_utcnow()
            db_session.add(user)
            db_session.commit()
            return redirect(url_for('index'))
        flash('Unable to login - please contact support.', 'error')
        return redirect(url_for('login'))

    return render_template('forms/reset_password.html', form=form,
                           payload=payload, title='Reset password')


@app.route('/logout/')
@login_required
def logout():
    logout_user()
    # display message to user
    flash('You have been securely logged out.')
    return redirect(url_for('index'))


###############################################################################

@app.route('/privacy/')
def privacy():
    return render_template('privacy.html', title='Privacy statement')


@app.route('/styleguide/')
def styleguide():
    return render_template('styleguide.html')


@app.errorhandler(500)
def internal_server_error(error):
    db_session.rollback()
    return render_template('500.html', event_id=g.sentry_event_id,
                           public_dsn=sentry.client.get_public_dsn('https'))
