'''
Custom exceptions.
'''


class BookingExpiredError(Exception):
    '''All new booking attendances were inserted > 8 minutes ago.'''
    pass


class NoNewAttendancesError(Exception):
    '''Booking has no new attendances.'''
    pass


class ObjectCancelledError(Exception):
    '''You can't modify a cancelled object.'''
    pass


class MultipleBookingsError(Exception):
    '''User already has a booking for the course.'''
    pass
