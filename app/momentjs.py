from jinja2 import Markup


# TODO: move to utils or a dedicated Jinja template functions file.
class moment(object):
    def __init__(self, timestamp):
        self.timestamp = timestamp

    def render(self, format):
        return Markup('<script>\ndocument.write(moment("{}").{});\n</script>'
                      .format(self.timestamp, format))

    def format(self, fmt):
        return self.render('format("{}")'.format(fmt))

    def utc_format(self, fmt):
        return self.render('utc().format("{}")'.format(fmt))

    def calendar(self):
        return self.render('calendar()')

    def fromNow(self):
        return self.render('fromNow()')
