import json
from datetime import datetime as dt
from datetime import timedelta

from cloudinary import uploader
import pytz
from sqlalchemy import Column, Boolean, DateTime, Integer, Numeric, \
    SmallInteger, String, ForeignKey, TypeDecorator
from sqlalchemy.orm import relationship

from app import app
from app.database import Base, db_session
from .config import BOOKING_LOCK
from .constants import Privacy, Status, Role
from .decorators import async
from .exceptions import BookingExpiredError, NoNewAttendancesError, \
    ObjectCancelledError, MultipleBookingsError
from .utils import friendly_url, new_identifier, aware_utcnow


class UTCDateTime(TypeDecorator):
    '''
    Throws error if incoming data is unaware.
    Always returns UTC-aware datetime objects.
    See: https://stackoverflow.com/a/2528453
    '''
    impl = DateTime

    def process_bind_param(self, value, engine):
        if value is not None:
            return value.astimezone(pytz.utc)

    def process_result_value(self, value, engine):
        if value is not None:
            return dt(value.year, value.month, value.day, value.hour,
                      value.minute, value.second, value.microsecond,
                      tzinfo=pytz.utc)


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    email = Column(String(256), unique=True)
    name = Column(String(128))
    password = Column(String(256))
    last_auth = Column(UTCDateTime, default=aware_utcnow)
    role = Column(SmallInteger, default=Role.USER.value)
    status = Column(SmallInteger, default=Status.NEW.value)
    # settings
    timezone = Column(String(32), default='Pacific/Auckland')
    tfa_enabled = Column(Boolean, default=False)     # 2-factor auth
    tfa_secret = Column(String(16), default=None)    # 2FA secret key
    tfa_backup = Column(String(512), default=None)   # 2FA backup codes
    # timestamps
    update_date = Column(UTCDateTime, default=aware_utcnow,
                         onupdate=aware_utcnow)
    insert_date = Column(UTCDateTime, default=aware_utcnow)
    # relationships
    courses = relationship('Course', backref='user')
    bookings = relationship('Booking', backref='user')
    organisers = relationship('Organiser', backref='user')

    def __init__(self, email=None, name=None, password=None,
                 role=Role.USER.value):
        self.email = email
        self.name = name
        self.password = password
        self.role = role

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return self.status == Status.ACTIVE.value

    @property
    def is_anonymous(self):
        return False

    def newly_authenticated(self):
        return self.last_auth > (aware_utcnow() - timedelta(seconds=10))

    def get_id(self):
        return str(self.id)

    @staticmethod
    def get_user(id=None):
        return User.query.filter_by(id=id).first()

    @staticmethod
    def get_user_id(email=None):
        return User.query.filter_by(email=email).first().id

    def getStatus(self):
        return Status(self.status).name.lower()

    def getRole(self):
        return Role(self.role).name.lower()

    def get_course(self, course_id):
        '''
        From among courses created by this user.
        '''
        courses = {c.identifier: c for c in self.courses}
        return courses[course_id]

    def get_booking(self, course_id):
        '''
        Get booking by course_id from among user's active bookings.
        '''
        bookings = {b.course_id: b for b in self.active_bookings()}
        return bookings[course_id]

    def get_attendance(self, workshop_id):
        '''
        Get attendance by workshop_id from among user's active attendances.
        '''
        attendances = {a.workshop_id: a for a in self.active_attendances()}
        return attendances[workshop_id]

    def active_bookings(self):
        bookings = [
            b for b in self.bookings if b.status == Status.ACTIVE.value]
        return sorted(bookings, key=lambda course: course.id, reverse=True)

    def active_attendances(self):
        return [a for a in self.attendances if a.status == Status.ACTIVE.value]

    def booked_courses(self):
        return [b.course_id for b in self.active_bookings()]

    def course_bookings(self, course_id):
        '''List all bookings the user has for a given course.'''
        return Booking.query.filter(
            Booking.course_id == course_id, Booking.user_id == self.id).all()

    def attended_workshops(self):
        return [a.workshop for a in self.active_attendances()]

    def manages_attendance(self, attendance_id):
        '''
        Is the given attendance for a course managed by the current user?
        '''
        attendance = Attendance.query.filter_by(id=attendance_id).first()
        return attendance.booking.course_id in [c.id for c in self.courses]

    def attending_full_course(self, course):
        '''
        Is the user attending all workshops for a given course?
        '''
        return set(self.attended_workshops()) >= set(course.workshops)

    def __repr__(self):
        return u'<User {} {}>'.format(self.id, self.email)


class Organiser(Base):
    __tablename__ = 'organisers'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    email = Column(String(256))
    phone = Column(String(32))
    name = Column(String(256))
    description = Column(String(512))
    # timestamps
    update_date = Column(
        UTCDateTime, default=aware_utcnow, onupdate=aware_utcnow)
    insert_date = Column(UTCDateTime, default=aware_utcnow)
    # relationships
    courses = relationship('Course', backref='organiser')

    def __init__(self, email=None, phone=None, name=None, description=None):
        self.email = email
        self.phone = phone
        self.name = name
        self.description = description


class Course(Base):
    __tablename__ = 'courses'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    organiser_id = Column(Integer, ForeignKey('organisers.id'))
    identifier = Column(String(16), unique=True)
    name = Column(String(256))
    description = Column(String(1024))
    image = Column(String(64), default=None)
    privacy = Column(SmallInteger, default=Privacy.PRIVATE.value)
    status = Column(SmallInteger, default=Status.NEW.value)
    booking_open = Column(UTCDateTime, default=aware_utcnow)
    booking_close = Column(UTCDateTime)
    # timestamps
    update_date = Column(UTCDateTime, default=aware_utcnow,
                         onupdate=aware_utcnow)
    insert_date = Column(UTCDateTime, default=aware_utcnow)
    # relationships
    address = relationship('Address', uselist=False, back_populates='course')
    workshops = relationship('Workshop', backref='course')
    bookings = relationship('Booking', backref='course')
    questions = relationship('Question', backref='course')

    def __init__(self, organiser_id=None, name=None, description=None,
                 image=None, privacy=Privacy.PRIVATE.value,
                 status=Status.NEW.value):
        self.organiser_id = organiser_id
        self.identifier = new_identifier(8, case='both')
        self.name = name
        self.description = description
        self.image = image
        self.privacy = privacy
        self.status = status

    @staticmethod
    def get_course(identifier=None):
        return Course.query.filter_by(identifier=identifier).first()

    def slug(self):
        return friendly_url(self.name)

    def set_active(self):
        """Set course status to ACTIVE."""
        self.status = Status.ACTIVE.value

    @async
    def set_image(self, image):
        with app.app_context():
            res = uploader.upload(image)
            self.image = res['public_id']
            db_session.add(self)
            db_session.commit()

    def get_workshop(self, workshop_id):
        workshops = {w.id: w for w in self.workshops}
        return workshops[workshop_id]

    def get_question(self, question_id):
        questions = {q.id: q for q in self.questions}
        return questions[question_id]

    def active_workshops(self):
        """Return list of active workshops belonging to self."""
        return [w for w in self.workshops if w.status == Status.ACTIVE.value]

    def sorted_workshops(self):
        workshops = self.active_workshops()
        if len(workshops) > 1:
            return sorted(workshops, key=lambda w: w.start)
        return workshops

    @property
    def start(self):
        # Return start of first-starting workshop.
        starttimes = [w.start for w in self.active_workshops()]
        if starttimes:
            return min(starttimes)
        return None

    @property
    def end(self):
        # Return start of last-starting workshop.
        starttimes = [w.start for w in self.active_workshops()]
        if starttimes:
            return max(starttimes)
        return None

    def remaining(self):
        remaining = sum(w.remaining() for w in self.active_workshops())
        return max(0, remaining)

    def has_started(self):
        # Return True if the first-starting workshop started before today.
        return self.start and self.start < aware_utcnow()

    def booking_is_open(self):
        return (aware_utcnow() > self.booking_open and not
                self.booking_is_closed())

    def booking_is_closed(self):
        return self.booking_close and aware_utcnow() > self.booking_close

    def booking_will_open(self):
        return not self.booking_is_open() and not self.booking_is_closed()

    def has_finished(self):
        # Return True if the last-ending workshop finished before today.
        return self.end and self.end < aware_utcnow()

    def sold_out(self):
        return self.remaining() < 1

    def can_be_booked(self, polite=False):
        """Return True or False depending on whether self can be booked.

        Prevent new bookings for a course which is not open for booking, has
        closed for booking, is no longer active, has finished, or has sold out.
        """
        try:
            if not self.booking_is_open():
                raise Exception('The course is not yet open for booking.')
            if self.booking_is_closed():
                raise Exception('The course has closed for booking.')
            if self.status == Status.INACTIVE.value:
                raise Exception('The course is no longer active.')
            if self.has_finished():
                raise Exception('All classes have now finished.')
            if self.sold_out():
                raise Exception('The course has sold out.')
        except Exception:
            if polite:
                return False
            raise
        return True

    def next_workshop(self):
        workshops = [w for w in self.active_workshops()
                     if w.start >= aware_utcnow()]
        workshops.sort(key=lambda w: w.start)
        if workshops:
            return workshops[0]
        return None

    def places(self):
        '''
        Sum of places available on all the course's classes.
        '''
        return sum(w.places for w in self.active_workshops())

    def taken(self):
        try:
            return self.places() - self.remaining()
        except Exception as e:
            return e

    def __repr__(self):
        return u'<Course {} {}>'.format(self.id, self.name)


class Address(Base):
    __tablename__ = 'addresses'
    id = Column(Integer, primary_key=True)
    course_id = Column(Integer, ForeignKey('courses.id'))
    name = Column(String(128))
    line1 = Column(String(256))
    line2 = Column(String(256))
    city = Column(String(128))
    region = Column(String(128))
    country = Column(String(128))
    postcode = Column(String(16))
    status = Column(SmallInteger, default=Status.NEW.value)
    # timestamps
    update_date = Column(
        UTCDateTime, default=aware_utcnow, onupdate=aware_utcnow)
    insert_date = Column(UTCDateTime, default=aware_utcnow)
    # relationships
    course = relationship('Course', back_populates="address")

    def __init__(self, name=None, line1=None, line2=None, city=None,
                 region=None, country=None, postcode=None):
        self.name = name
        self.line1 = line1
        self.line2 = line2
        self.city = city
        self.region = region
        self.postcode = postcode
        self.country = country

    def one_line(self):
        address = [self.name, self.line1, self.line2, self.city, self.region,
                   self.postcode, self.country]
        return ', '.join(i for i in address if i)

    def __repr__(self):
        return u'<Address {}, {}, {}>'.format(self.name, self.line2, self.city)


class Workshop(Base):
    __tablename__ = 'workshops'
    id = Column(Integer, primary_key=True)
    course_id = Column(Integer, ForeignKey('courses.id'))
    name = Column(String(256))
    description = Column(String(512))
    start = Column(UTCDateTime)
    end = Column(UTCDateTime)
    places = Column(SmallInteger)
    min_places = Column(SmallInteger, default=0)
    max_places = Column(SmallInteger)
    price = Column(Numeric(scale=2))
    status = Column(SmallInteger, default=Status.ACTIVE.value)
    # timestamps
    update_date = Column(
        UTCDateTime, default=aware_utcnow, onupdate=aware_utcnow)
    insert_date = Column(UTCDateTime, default=aware_utcnow)

    def __init__(self, name=None, description=None, start=None, end=None,
                 places=None, max_places=None, price=None):
        self.name = name
        self.description = description
        self.places = places
        self.max_places = max_places
        self.price = price
        self.start = start
        self.end = end
        self.status = Status.ACTIVE.value

    @staticmethod
    def get_workshop(id=None):
        return Workshop.query.filter_by(id=id).first()

    def active_attendances(self):
        return [a for a in self.attendances if a.status == Status.ACTIVE.value]

    def display_price(self, symbol='$'):
        if self.price == 0:
            return 'FREE'
        return "{}{:,.2f}".format(symbol, self.price)

    def remaining(self):
        """Return number of remaining places available on the course.

        Subtracts active attendances from total seats on the course.
        """
        active = Attendance.query.filter_by(
            workshop_id=self.id, status=Status.ACTIVE.value).all()
        active = sum(a.quantity for a in active)
        # Subtract reserved attendances (last Xmin) from places available.
        eight_min_ago = aware_utcnow() - timedelta(minutes=BOOKING_LOCK)
        pending = Attendance.query.filter(
            Attendance.workshop_id == self.id,
            Attendance.status == Status.NEW.value,
            Attendance.insert_date > eight_min_ago).all()
        pending = sum(p.quantity for p in pending)
        return self.places - (active + pending)

    def has_started(self):
        # Return True if workshop started before now.
        return self.start and self.start < aware_utcnow()

    def has_finished(self):
        # Return True if workshop finished before now.
        return self.start and self.start < aware_utcnow()

    def sold_out(self):
        return self.remaining() < 1

    def can_be_booked(self, polite=False):
        """Return True or False depending on whether self can be booked."""
        try:
            self.course.can_be_booked()
            if self.status == Status.INACTIVE.value:
                raise Exception('The class is no longer active and cannot be '
                                'booked.')
            if self.has_finished():
                raise Exception(u'The class "{}" has finished and can no '
                                'longer be attended.'.format(self.name))
            if self.sold_out():
                raise Exception('There are currently no places available in '
                                'the class.')
        except Exception:
            if polite:
                return False
            raise
        return True

    def cancel(self):
        """Set attendance statuses to inactive."""
        for a in self.attendances:
            a.status = Status.INACTIVE.value
        # Set workshop status to inactive.
        self.status = Status.INACTIVE.value

    def __repr__(self):
        return u'<Workshop {} {}>'.format(self.id, self.name)


class Attendance(Base):
    __tablename__ = 'attendances'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    workshop_id = Column(Integer, ForeignKey('workshops.id'))
    booking_id = Column(Integer, ForeignKey('bookings.id'))
    quantity = Column(SmallInteger, default=1)
    price = Column(Numeric(scale=2))
    status = Column(SmallInteger, default=Status.NEW.value)
    # timestamps
    update_date = Column(UTCDateTime, default=aware_utcnow,
                         onupdate=aware_utcnow)
    insert_date = Column(UTCDateTime, default=aware_utcnow)
    # relationships
    user = relationship('User', backref='attendances')
    workshop = relationship('Workshop', backref='attendances')

    def __init__(self, workshop_id=None, quantity=None):
        workshop = Workshop.query.filter_by(id=workshop_id).first()
        if not workshop:
            raise Exception('Invalid class id provided.')
        self.workshop_id = workshop.id
        self.price = workshop.price
        self.quantity = quantity

    def get_course(self):
        return self.workshop.course

    def cancel(self):
        # Prevent cancelling attendances for cancelled bookings.
        if self.booking.status != Status.ACTIVE.value:
            raise Exception('You cannot cancel an attendance for a booking '
                            'which has been cancelled.')
        # If there's only one attendance cancel the booking.
        if len(self.booking.active_attendances()) == 1:
            self.booking.cancel()
            return self.booking
        else:
            # Set attendance status to inactive.
            self.status = Status.INACTIVE.value
            return self

    def is_expired(self):
        return (self.insert_date + timedelta(
            minutes=BOOKING_LOCK)) < aware_utcnow()

    def __repr__(self):
        return u'<Attendance {} for \'{}\', {}>'.format(
            self.id, self.workshop.name, Status(self.status).name)


class Booking(Base):
    __tablename__ = 'bookings'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    course_id = Column(Integer, ForeignKey('courses.id'))
    identifier = Column(String(8), unique=True)
    price = Column(Numeric(scale=2), default=0)
    status = Column(SmallInteger, default=Status.NEW.value)
    information = Column(String(2048))
    # timestamps
    update_date = Column(UTCDateTime, default=aware_utcnow,
                         onupdate=aware_utcnow)
    insert_date = Column(UTCDateTime, default=aware_utcnow)
    # relationships
    attendances = relationship('Attendance', backref='booking')

    def __init__(self, user_id=None, course_id=None):
        self.user_id = user_id
        self.course_id = course_id
        self.identifier = new_identifier(8, case='upper')

    def get_user(self):
        return User.get_user(id=self.user_id)

    def get_course(self):
        return Course.query.filter_by(id=self.course_id).first()

    def get_information(self):
        # Exception handling because json might chow down on a None.
        try:
            return json.loads(self.information)
        except TypeError:
            return {}

    def reserved_until(self):
        '''
        Reserved until X minutes past oldest NEW attendance on booking. This
        permits NEW attendances to be added to an existing booking.
        '''
        return min(a.insert_date for a in self.new_attendances()) + timedelta(
            minutes=BOOKING_LOCK)

    def validate(self):
        course = self.get_course()
        # Ensure new attendances were created in last BOOKING_LOCK minutes.
        if self.expired_attendances():
            raise BookingExpiredError('The booking we were holding for you '
                                      'expired after {} minutes.  Please try '
                                      'again'.format(BOOKING_LOCK))
        # Ensure booking has new attendances on it.
        if not self.new_attendances():
            raise NoNewAttendancesError(
                'Please select the classes you wish to attend.')
        # Prevent modification of cancelled bookings.
        if self.status not in [Status.NEW.value, Status.ACTIVE.value]:
            raise ObjectCancelledError(
                'The booking cannot be changed as it has been cancelled.')
        # If active, course must be active and running for booking changes.
        if self.status == Status.ACTIVE.value:
            message = 'The booking cannot be changed as the course has '
            if course.status != Status.ACTIVE.value:
                raise ObjectCancelledError(message + 'been cancelled.')
            if course.has_finished():
                raise Exception(message + 'finished.')
        # Ensure user doesn't already have an ACTIVE booking for this course.
        if self.user_id and self.course_id in self.get_user().booked_courses():
            raise MultipleBookingsError(
                'You already have a booking for this course.')

    def number_of_items(self):
        return sum(a.quantity for a in self.attendances)

    def sub_total(self):
        return sum(a.workshop.price * a.quantity for a in self.attendances)

    def new_attendances(self):
        return [a for a in self.attendances if a.status == Status.NEW.value]

    def active_attendances(self):
        return [a for a in self.attendances if a.status == Status.ACTIVE.value]

    def inactive_attendances(self):
        return [
            a for a in self.attendances if a.status == Status.INACTIVE.value]

    def new_and_active(self):
        return [
            a for a in self.attendances if a.status != Status.INACTIVE.value]

    def valid_attendances(self):
        """Return attendances which aren't inactive.

        Used when creating or modifying a new or existing booking.
        """
        return [
            a for a in self.attendances if a.status != Status.INACTIVE.value]

    def expired_attendances(self):
        return [a for a in self.new_attendances() if a.is_expired()]

    def expired_and_inactive_attendances(self):
        return self.expired_attendances() + self.inactive_attendances()

    def get_workshop(self, workshop_id):
        workshops = {a.workshop_id: a for a in self.attendances}
        return workshops[workshop_id]

    def set_user(self, user_id):
        '''
        Set the user id on the booking and child attendances.
        '''
        self.user_id = user_id
        for a in self.attendances:
            a.user_id = user_id

    def confirm(self):
        # Activate new attendances.
        for a in self.new_attendances():
            a.status = Status.ACTIVE.value
        # Set booking status to active.
        self.status = Status.ACTIVE.value
        self.price = self.sub_total()

    def cancel(self):
        if self.status == Status.INACTIVE.value:
            raise Exception('Booking already cancelled.')
        # Set attendance statuses to inactive.
        for a in self.attendances:
            a.status = Status.INACTIVE.value
        # Set booking status to inactive.
        self.status = Status.INACTIVE.value

    def __repr__(self):
        return u'<Booking {} {}, {}>'.format(
            self.identifier, self.user, Status(self.status).name)


class Question(Base):
    __tablename__ = 'questions'
    id = Column(Integer, primary_key=True)
    course_id = Column(Integer, ForeignKey('courses.id'))
    name = Column(String(128))
    type = Column(String(16))
    required = Column(Boolean)
    # timestamps
    update_date = Column(
        UTCDateTime, default=aware_utcnow, onupdate=aware_utcnow)
    insert_date = Column(UTCDateTime, default=aware_utcnow)

    def __init__(self, name=None, type='input', required=False):
        self.name = name
        self.type = type
        self.required = required
