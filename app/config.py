import os
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_TRACK_MODIFICATIONS = True

APP_NAME = 'Coursebasics'

# Environment variables only available on heroku.
try:
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    SECRET_KEY = os.environ['SECRET_KEY']
    DEBUG = False
    SENTRY_DSN = os.environ['SENTRY_DSN']
    SENTRY_ENVIRONMENT = 'production'
    MAILGUN_BASE_URL = 'https://api.mailgun.net/v3/mg.{}.com'.format(
        APP_NAME.lower())
    MAILGUN_API_KEY = os.environ['MAILGUN_API_KEY']
    SESSION_COOKIE_SECURE = True
    REMEMBER_COOKIE_SECURE = True
    CLOUDINARY_CLOUD_NAME = os.environ['CLOUDINARY_CLOUD_NAME']
    CLOUDINARY_API_KEY = os.environ['CLOUDINARY_API_KEY']
    CLOUDINARY_API_SECRET = os.environ['CLOUDINARY_API_SECRET']
except:
    SQLALCHEMY_DATABASE_URI = 'postgresql://monday:somethingfunny@localhost/monday'
    SECRET_KEY = 'U\xa0m\xe4\xf6\x1f\xf5\xbb\r\xd3\xca~\xdc\x13w\x98*\xd0\xe0\xd5\xad\x8fo\x0b'
    DEBUG = True
    SENTRY_DSN = ''
    SENTRY_ENVIRONMENT = 'development'
    MAILGUN_BASE_URL = 'https://api.mailgun.net/v3/sandboxcf4e4deb1ca44618a5931ca5b79e0641.mailgun.org'
    MAILGUN_API_KEY = 'key-19b78a65fa3380829438df07db43033e'
    CLOUDINARY_CLOUD_NAME = ''
    CLOUDINARY_API_KEY = ''
    CLOUDINARY_API_SECRET = ''

WTF_CSRF_ENABLED = True
PERMANENT_SESSION_LIFETIME = timedelta(hours=4)
SESSION_COOKIE_HTTPONLY = True
REMEMBER_COOKIE_HTTPONLY = True
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

# How many minutes bookings are reserved for a user during registration.
BOOKING_LOCK = 8

# Email stuff
ADMINS = ['weldu@yandex.com']
SUPPORT = '{} Support <support@{}.com>'.format(APP_NAME, APP_NAME.lower())
NOTIFICATIONS = '{} <no-reply@{}.com>'.format(APP_NAME, APP_NAME.lower())
