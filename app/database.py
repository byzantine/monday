#!venv/bin/python
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

from .config import SQLALCHEMY_DATABASE_URI
from .constants import Status, Role


'''
http://flask.pocoo.org/docs/0.12/patterns/sqlalchemy/
'''

engine = create_engine(
    SQLALCHEMY_DATABASE_URI,
    convert_unicode=True,
    connect_args={"options": "-c timezone=utc"},
)
db_session = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    # Import all modules here that might define models so that they will be
    # registered properly on the metadata.  Otherwise you will have to import
    # them first before calling init_db()
    from app import models
    Base.metadata.create_all(bind=engine)

    # create default accounts
    hugo = models.User(email='windlab@gmail.com', name='Hugo')
    hugo.role = Role.ADMIN.value
    hugo.status = Status.ACTIVE.value
    hugo.password = 'pbkdf2:sha1:1000$uiIWnDPD$a76786da097e77641c407ceb253040ce14e3c773'
    db_session.add_all([hugo])
    db_session.commit()


def drop_db():
    from app import models
    Base.metadata.drop_all(bind=engine)
