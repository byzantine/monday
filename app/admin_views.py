from flask import abort, g
from flask_admin.contrib.sqla import ModelView
from flask_admin.form import SecureForm
from app import admin
from .constants import Role
from .database import db_session
from .models import User, Course, Workshop, Attendance, Booking


class AdminView(ModelView):
    form_base_class = SecureForm

    def is_accessible(self):
        return g.user.is_authenticated and g.user.role == Role.ADMIN.value

    def inaccessible_callback(self, name, **kwargs):
        abort(404)


admin.add_view(AdminView(User, db_session))
admin.add_view(AdminView(Course, db_session))
admin.add_view(AdminView(Workshop, db_session))
admin.add_view(AdminView(Booking, db_session))
admin.add_view(AdminView(Attendance, db_session))
