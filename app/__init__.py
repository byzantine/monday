from flask import Flask
import flask_admin as admin
from flask_admin import expose
from flask_login import LoginManager
from flask_sslify import SSLify
from raven.contrib.flask import Sentry
from werkzeug.contrib.fixers import ProxyFix
import cloudinary
from .config import SENTRY_DSN, CLOUDINARY_CLOUD_NAME, CLOUDINARY_API_KEY, \
    CLOUDINARY_API_SECRET
from .decorators import admin_only
from .momentjs import moment


app = Flask(__name__)
app.config.from_object('app.config')
app.jinja_env.globals['moment'] = moment
app.wsgi_app = ProxyFix(app.wsgi_app)

# Redirect all incoming requests to HTTPS.
sslify = SSLify(app)

# Flask-Login
lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
# TODO: BREAKS WTFORMS CSRF PROTECTION
# Destroy session if User-Agent or IP changes.
# lm.session_protection = "strong"
lm.session_protection = "basic"


# Flask-Admin
# Create customized index view class for admin.
class MyAdminIndexView(admin.AdminIndexView):

    @expose('/')
    @admin_only
    def index(self):
        return super(MyAdminIndexView, self).index()


admin = admin.Admin(app, index_view=MyAdminIndexView())

# Sentry error logging
sentry = Sentry(app, dsn=SENTRY_DSN)

# Update cloudinary config.
cloudinary.config(
    cloud_name=CLOUDINARY_CLOUD_NAME,
    api_key=CLOUDINARY_API_KEY,
    api_secret=CLOUDINARY_API_SECRET,
)

# import statement at end to avoid circular reference; views module imports app
from app import views, admin_views
