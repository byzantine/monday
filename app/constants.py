from enum import Enum


class Role(Enum):
    """User roles."""
    ADMIN = 0
    STAFF = 1
    USER = 2


class Status(Enum):
    """Activity levels for different entities."""
    INACTIVE = 0
    NEW = 1
    ACTIVE = 2


class Privacy(Enum):
    """Privacy levels."""
    PRIVATE = 0
    PUBLIC = 1
