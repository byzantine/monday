import unittest
from datetime import timedelta
from app.models import Course
from app.utils import aware_utcnow


class TestCourse(unittest.TestCase):

    def setUp(self):
        self.course = Course(name='Yoga for Septuagenarians')

    def tearDown(self):
        del(self.course)

    def test_booking_is_open(self):
        # Positive:
        self.course.booking_open = aware_utcnow() - timedelta(days=1)
        self.assertTrue(self.course.booking_is_open())
        # Negative:
        self.course.booking_open = aware_utcnow() + timedelta(days=1)
        self.assertFalse(self.course.booking_is_open())

    def test_booking_is_closed(self):
        # Null value (default):
        self.course.booking_close = None
        self.assertFalse(self.course.booking_is_closed())
        # Positive:
        self.course.booking_close = aware_utcnow() - timedelta(days=1)
        self.assertTrue(self.course.booking_is_closed())
        # Negative:
        self.course.booking_close = aware_utcnow() + timedelta(days=1)
        self.assertFalse(self.course.booking_is_closed())

    def test_booking_will_open(self):
        # Course booking open, no close specified.
        self.course.booking_close = None
        self.course.booking_open = aware_utcnow() - timedelta(days=1)
        self.assertFalse(self.course.booking_will_open())
        # Course booking open, booking close in the future.
        self.course.booking_close = aware_utcnow() + timedelta(days=1)
        self.course.booking_open = aware_utcnow() - timedelta(days=1)
        self.assertFalse(self.course.booking_will_open())
        # Course booking open in the future, no close specified.
        self.course.booking_close = None
        self.course.booking_open = aware_utcnow() + timedelta(days=1)
        self.assertTrue(self.course.booking_will_open())
        # Course booking open in the future, booking close in the future.
        self.course.booking_close = aware_utcnow() + timedelta(days=1)
        self.course.booking_open = aware_utcnow() + timedelta(days=1)
        self.assertTrue(self.course.booking_will_open())


if __name__ == '__main__':
    unittest.main()
