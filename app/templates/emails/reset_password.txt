Someone requested a passphrase reset for the account {{ email }}

If that was you, please follow the link below:
{{ link }}
This link will expire in one hour.

If you didn't request a passphrase reset, you can ignore this email.
