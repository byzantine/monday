from functools import wraps
from threading import Thread

from flask import abort, g
from flask_login import login_required

from .constants import Role


def async(f):
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper


def admin_only(f):
    @wraps(f)
    @login_required
    def decorated_function(*args, **kwargs):
        if g.user.role != Role.ADMIN.value:
            abort(404)
        return f(*args, **kwargs)
    return decorated_function
