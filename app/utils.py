# This Python file uses the following encoding: utf-8
from datetime import datetime
import random
from string import ascii_lowercase, ascii_uppercase, digits

import pytz
from pytz import timezone

from app import app
from .config import CLOUDINARY_CLOUD_NAME


def new_identifier(length, case=None):
    charset = ascii_lowercase + digits
    if case == 'upper':
        charset = ascii_uppercase + digits
    if case == 'both':
        charset = ascii_uppercase + ascii_lowercase + digits
    # Remove O & 0, I, l & 1 to prevent mistakes.
    charset = charset.translate({ord(c): None for c in 'O0Il1'})
    return ''.join(random.sample(charset, length))


def remap_international_char_to_ascii(c):
    try:
        s = str(c).lower()
    except BaseException:
        return ''
    if s in u'àåáâäãåą':
        return 'a'
    elif s in u'èéêëę':
        return 'e'
    elif s in u'ìíîïı':
        return 'i'
    elif s in u'òóôõöøőð':
        return 'o'
    elif s in u'ùúûüŭů':
        return 'u'
    elif s in u'çćčĉ':
        return 'c'
    elif s in u'żźž':
        return 'z'
    elif s in u'śşšŝ':
        return 's'
    elif s in u'ñń':
        return 'n'
    elif s in u'ýÿ':
        return 'y'
    elif s in u'ğĝ':
        return 'g'
    elif c == 'ř':
        return 'r'
    elif c == 'ł':
        return 'l'
    elif c == 'đ':
        return 'd'
    elif c == 'ß':
        return 'ss'
    elif c == 'Þ':
        return 'th'
    elif c == 'ĥ':
        return 'h'
    elif c == 'ĵ':
        return 'j'
    return ''


@app.template_filter('friendly_url')
def friendly_url(title):
    if title is None:
        return None
    maxlen = 80
    length = len(title)
    prevdash = False
    new = ''
    for idx, c in enumerate(title):
        if c in ascii_lowercase or c in digits:
            new = ''.join([new, c])
            prevdash = False
        elif c in ascii_uppercase:
            new = ''.join([new, c.lower()])
            prevdash = False
        elif c in [' ', ',', '.', '/', '\\', '-', '_', '=', '(', ')']:
            if not prevdash and length > 0:
                new = ''.join([new, '-'])
                prevdash = True
        # If character out of ascii range.
        elif lambda c: len(c) == len(c.encode()):
            prevlen = len(new)
            new = ''.join([new, remap_international_char_to_ascii(c)])
            if prevlen != len(new):
                prevdash = False
        if idx == maxlen:
            break
    if prevdash:
        new = new[0:-1]
    if new == '':
        return None
    return new


def display_currency(amount=0):
    return '${:,.2f}'.format(amount)


def make_aware(dt, tz):
    '''
    Update naive datetime object to be timezone aware.
    '''
    tz = timezone(tz)
    return tz.localize(dt)


def tz_to_utc(dt, tz):
    '''
    Convert given datetime object to UTC.
    '''
    user_tz = timezone(tz)
    loc_dt = user_tz.localize(dt)
    return loc_dt.astimezone(pytz.utc)


def utc_to_tz(dt, tz_string):
    """Return given UTC datetime object localised to given timezone."""
    return dt.astimezone(timezone(tz_string))


def aware_utcnow():
    '''
    See:
    https://docs.python.org/3.6/library/datetime.html#datetime.datetime.utcnow
    '''
    return datetime.now(pytz.utc)


# Convert UTC datetime to NZDT and return formatted string.
@app.template_filter('nzdt')
def nzdt(timestamp, format='%H:%M %p'):
    nzdt = pytz.timezone('Pacific/Auckland')
    try:
        return timestamp.astimezone(nzdt).strftime(format)
    except AttributeError:
        return ''


@app.template_filter('format_price')
def format_price(price):
    if price == 0:
        return 'FREE'
    return '${:,.2f}'.format(price)


@app.template_filter('cloudinary')
def cloudinary(public_id, height=None, width=None):
    code = (
        '<img data-src="http://res.cloudinary.com/{cloud_name}/image/upload/'
        'c_fill,h_{height},w_{width}/{public_id}" height="{height}" '
        'width="{width}" class="cld-responsive"/>'.format(
            cloud_name=CLOUDINARY_CLOUD_NAME,
            height=height,
            width=width,
            public_id=public_id,
        )
    )
    return code
