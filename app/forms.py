from datetime import datetime
from decimal import Decimal

from flask import request, url_for, redirect
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from iso3166 import countries
from urllib.parse import urlparse, urljoin
from werkzeug import check_password_hash
from wtforms import FieldList, FormField, StringField, BooleanField, \
    PasswordField, SelectField, TextAreaField, IntegerField, RadioField
from wtforms import Form as NoCsrfForm
from wtforms.validators import Length, InputRequired, EqualTo, Email, \
    StopValidation, Optional, ValidationError
from wtforms.widgets import HiddenInput

from .config import ALLOWED_EXTENSIONS
from .constants import Status
from .emails import send_dupe_email
from .models import User, Workshop


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


def get_redirect_target():
    for target in request.args.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target


# VALIDATORS
def currency(form, field):
    try:
        Decimal(field.data)
    except BaseException:
        raise StopValidation('Value must be in decimal format')
    if Decimal(field.data) < 0:
        raise StopValidation('Value must be greater than zero')


def is_positive(form, field):
    try:
        int(field.data)
    except BaseException:
        raise StopValidation('Number must be positive or zero')
    if int(field.data) < 0:
        raise StopValidation('Number must be positive or zero')


def is_ddmmyyyy(form, field):
    # Only validate if a value has been provided.
    if field.data:
        try:
            datetime.strptime(field.data, '%d/%m/%Y')
        except BaseException:
            raise StopValidation('Date must be in \'DD/MM/YYYY\' format')


def is_not_historic(form, field):
    try:
        date = datetime.strptime(field.data, '%d/%m/%Y')
        date >= datetime.now().date()
    except BaseException:
        raise StopValidation('Date must occur in the future')


def make_optional(field):
    field.validators.insert(0, Optional())


class GreaterThanField(object):
    '''
    Compares the values of two fields.

    :param fieldname:
        The name of the other field to compare to.
    :param message:
        Error message to raise in case of a validation error.
    '''

    def __init__(self, fieldname):
        self.fieldname = fieldname

    def __call__(self, form, field):
        try:
            if field.data <= form[self.fieldname].data:
                raise StopValidation('Class end must be after class start')
        except BaseException:
            raise StopValidation('Class end must be after class start')


COUNTRIES = [(c.name, c.name) for c in countries]
COUNTRIES.insert(0, ('Select a country', 'Select a country'))
hours = [
    (0, '12:00 midnight'), (1, '1:00am'), (2, '2:00am'), (3, '3:00am'),
    (4, '4:00am'), (5, '5:00am'), (6, '6:00am'), (7, '7:00am'),
    (8, '8:00am'), (9, '9:00am'), (10, '10:00am'), (11, '11:00am'),
    (12, '12:00 noon'), (13, '1:00pm'), (14, '2:00pm'), (15, '3:00pm'),
    (16, '4:00pm'), (17, '5:00pm'), (18, '6:00pm'), (19, '7:00pm'),
    (20, '8:00pm'), (21, '9:00pm'), (22, '10:00pm'), (23, '11:00pm')]

quantity = [(i, i) for i in range(0, 10 + 1)]


class AddressForm(NoCsrfForm):
    addr_name = StringField('Location', [Length(max=128)])
    addr_line1 = StringField('Address', [Length(max=256)])
    addr_line2 = StringField('Address 2', [Length(max=256)])
    addr_city = StringField('City', [Length(max=128)])
    addr_region = StringField('Region / State', [Length(max=128)])
    addr_country = SelectField('Country', [Length(max=128)], choices=COUNTRIES)
    addr_postcode = StringField('Postal Code', [Length(max=16)])


class WorkshopForm(NoCsrfForm):
    class_id = IntegerField(widget=HiddenInput(), default=0)
    class_name = StringField('Title', [InputRequired(), Length(max=256)])
    class_description = TextAreaField('Class description', [Length(max=512)])
    class_places = IntegerField('Available spaces',
                                [is_positive, InputRequired()])
    class_max_places = IntegerField('Spaces allowed per booking',
                                    [is_positive], default=1)
    class_price = StringField('Price', [currency, InputRequired()])
    class_date = StringField('Date', [is_ddmmyyyy, InputRequired(),
                                      Length(max=10)])
    class_start = SelectField('From', [InputRequired()], choices=hours,
                              coerce=int)
    class_end = SelectField('To',
                            [GreaterThanField('class_start'), InputRequired()],
                            choices=hours, coerce=int)


class AttendeeQuestion(NoCsrfForm):
    question_id = IntegerField(widget=HiddenInput(), default=0)
    question_name = StringField('Question', [Length(max=128)])
    question_required = BooleanField('Make input required', default=True)


class CourseForm(FlaskForm):
    course_name = StringField('Course title', [InputRequired(),
                                               Length(max=256)])
    course_location = FormField(AddressForm)
    description = TextAreaField('Course Description', [Length(max=1024)])
    privacy = RadioField('Listing privacy',
                         choices=[(1, 'Public'), (0, 'Private')], coerce=int,
                         default=0)
    classes = FieldList(FormField(WorkshopForm), min_entries=1)


class DateAndTime(NoCsrfForm):
    date = StringField('Date', [is_ddmmyyyy, Length(max=10)])
    time = SelectField('Time', choices=hours, coerce=int)


# Used by course_details() to add additional, optional attrs to a course.
class CourseDetailsForm(FlaskForm):
    image = FileField('Upload an image (jpg or png)',
                      [FileAllowed(ALLOWED_EXTENSIONS, 'Images only!')])
    booking_open = FormField(DateAndTime)
    booking_close = FormField(DateAndTime)
    questions = FieldList(FormField(AttendeeQuestion))


# The following two are used on course_show.
class CourseWorkshopForm(NoCsrfForm):
    workshop_id = IntegerField(widget=HiddenInput())
    quantity = SelectField('', choices=[(0, 0), (1, 1)], coerce=int, default=0)


class CourseRegistrationForm(FlaskForm):
    workshops = FieldList(FormField(CourseWorkshopForm))

    def validate_workshops(self, field):
        # Ensure at least one place is booked.
        if sum(w['quantity'] for w in self.workshops.data) < 1:
            raise ValidationError('Please select at least one class.')
        # Ensure all workshops belong to the same course.
        courses = {self.get_course(w['workshop_id']) for w in
                   self.workshops.data}
        if len(courses) > 1:
            raise ValidationError(
                'Classes must all belong to the same course.')

    def get_course(self, workshop_id):
        try:
            return Workshop.query.filter_by(id=workshop_id).first().course
        except AttributeError:
            return None


# The following two are used on course_register.
class OrderReviewNewUserQuestion(NoCsrfForm):
    @classmethod
    def append_field(cls, name, field):
        setattr(cls, name, field)
        return cls


class OrderReviewNewUserForm(FlaskForm):
    name = StringField('Your name', [Length(max=128), InputRequired()])
    email = StringField('Email', [Email(message='Invalid email address'),
                                  Length(max=128), InputRequired()])
    questions = FieldList(FormField(OrderReviewNewUserQuestion))


# ACCOUNT SETTINGS
class ChangePassword(FlaskForm):
    password = PasswordField('Current password', [Length(min=8, max=256),
                             InputRequired()])
    newpassword = PasswordField('New password', [Length(min=8, max=256),
                                InputRequired()])
    confirm = PasswordField('Password again', [
        Length(min=8, max=256), InputRequired(),
        EqualTo('newpassword', message='Passwords must match')])


# AUTHENTICATION
class RedirectForm(FlaskForm):
    next = HiddenInput()

    def __init__(self, *args, **kwargs):
        FlaskForm.__init__(self, *args, **kwargs)
        if not self.next.data:
            self.next.data = get_redirect_target() or ''

    def redirect(self, endpoint='index', **values):
        if is_safe_url(self.next.data):
            return redirect(self.next.data)
        target = get_redirect_target()
        return redirect(target or url_for(endpoint, **values))


class Signup(FlaskForm):
    name = StringField('Your name', [Length(max=128), InputRequired()])
    email = StringField('Email', [Email(message='Invalid email address'),
                                  Length(max=256), InputRequired()])
    password = PasswordField('Password', [Length(min=8, max=256),
                                          InputRequired()])
    confirm = PasswordField('Password again', [
        Length(min=8, max=256), InputRequired(),
        EqualTo('password', message='Passwords must match')])

    def validate_email(self, field):
        user = self.get_user()
        # If user status is NEW ignore so they can complete account creation.
        if user and user.status != Status.NEW.value:
            send_dupe_email(user.email)
            raise ValidationError(
                u'Someone with the email {} already has an account with us.  '
                'Did you mean to login?'.format(self.email.data))

    def get_user(self):
        return User.query.filter_by(email=self.email.data).first()


class Login(FlaskForm):
    '''
    Taken from:
    https://github.com/flask-admin/flask-admin/blob/master/examples/auth-flask-login/app.py
    '''
    email = StringField('Email', [Email(message='Invalid email address'),
                                  Length(max=256), InputRequired()])
    password = PasswordField('Password', [Length(min=8, max=256),
                                          InputRequired()])
    remember_me = BooleanField('remember_me', default=False)

    def validate_email(self, field):
        message = "Invalid email address or password."
        user = self.get_user()
        if user is None:
            raise ValidationError(message)

        # Get user to finish account setup if they have partial account.
        if not user.password or user.status == Status.NEW.value:
            raise StopValidation('New user account or no password set')

        # Use werkzeug to validate user's password.
        if not check_password_hash(user.password, self.password.data):
            raise ValidationError(message)

    def get_user(self):
        return User.query.filter_by(email=self.email.data).first()


class ForgotPassword(FlaskForm):
    email = StringField('Enter your email address',
                        [Email(message='Invalid email address'),
                         Length(max=256), InputRequired()])


class ResetPassword(FlaskForm):
    newpassword = PasswordField('New password', [Length(min=8, max=256),
                                                 InputRequired()])
